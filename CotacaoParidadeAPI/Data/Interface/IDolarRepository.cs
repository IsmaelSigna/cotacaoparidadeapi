﻿using CotacaoParidadeAPI.Data.Entities;
using CotacaoParidadeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CotacaoParidadeAPI.Data.Interface
{
    public interface IDolarRepository
    {
        IEnumerable<Dolar>  searchDolar    (DolarModel dolar);
        bool          insertDolar (DolarModel dolar);
        bool          updateDolar (DolarModel dolar);
        bool          deleteDolar (DolarModel dolar);
    }
}