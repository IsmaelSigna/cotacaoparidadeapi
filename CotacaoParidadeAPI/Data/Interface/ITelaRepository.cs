﻿using CotacaoParidadeAPI.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CotacaoParidadeAPI.Data.Interface
{
    
    public interface ITelaRepository
    {
        Tela carregarTela();
    }
    
}