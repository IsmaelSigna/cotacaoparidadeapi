﻿using CotacaoParidadeAPI.Data.Entities;
using CotacaoParidadeAPI.Data.Interface;
using CotacaoParidadeAPI.Models;
using Dapper;
using Signa.Library.Helpers;
using System;
using System.Collections.Generic;

namespace CotacaoParidadeAPI.Data.Repository
{
    public class DolarRepository : RepositoryBase, IDolarRepository
    {
        public IEnumerable<Dolar> searchDolar(DolarModel dolar)
        {
            using (var db = Connection)
            {
                string sql = @"
                    SELECT 
	                    DOLAR.DOLAR_ID		AS id,
	                    DOLAR.TAB_STATUS_ID	AS tabStatusId,
	                    DOLAR.DATA			AS data,
	                    DOLAR.VALOR_TURISMO	AS valorTurismo,
	                    DOLAR.VALOR_PARALELO	AS valorParalelo,
	                    DOLAR.VALOR_OFICIAL	AS valorOficial,
	                    DOLAR.val_performance	AS valPerformance,
	                    DOLAR.val_oficial		AS valOficial,
                        DOLAR.moeda_id        AS moedaId,
                        MOEDA.MOEDA             AS moedaDesc
	 
                    FROM 
	                    DOLAR 
                        INNER JOIN MOEDA ON DOLAR.MOEDA_ID = MOEDA.MOEDA_ID
                    WHERE
		                    (DATA               = @DATA             OR @DATA            IS NULL)
                        AND (dolar.MOEDA_ID = @MOEDA_ID OR @MOEDA_ID = 0 OR @MOEDA_ID IS NULL )
	                    AND	TAB_STATUS_ID <> 2
               ";

                return  db.Query<Dolar>
                (
                    sql,
                    param: new
                    {
                        ID = dolar.id,
                        DATA = dolar.data,
                        VALOR_TURISMO = dolar.valorTurismo,
                        VALOR_PARALELO = dolar.valorParalelo,
                        VALOR_OFICIAL =     dolar.valorOficial,
                        VAL_PERFORMANCE = dolar.valPerformance,
                        VAL_OFICIAL = dolar.valOficial,
                        MOEDA_ID = dolar.moedaId

                    }
                );
            }//using
        }//get

        public bool insertDolar(DolarModel dolar)
        {
            IEnumerable<RetornoBD> retorno = null;
            string sql =
            @"
                DECLARE @ID INT
                DECLARE @RETORNO INT
                DECLARE @DESC_DOLAR VARCHAR(100)
                DECLARE @DUPLICIDADE TABLE ( QTD_DOLAR INT)
                DECLARE @QTD_DOLAR INT

                INSERT INTO @DUPLICIDADE EXEC SP_050_ECR_DUP_DOLAR @DATA, NULL, @MOEDA_ID
                select @QTD_DOLAR = QTD_DOLAR from @duplicidade

                IF @MOEDA_ID = 0 
                    BEGIN
                        SET @MOEDA_ID = NULL
                    END
                
                IF @QTD_DOLAR = 0 
                    BEGIN

                        SELECT 
                            @ID = dolar_id 
                        FROM infra_ids
    
                        SET @ID = @ID + 1

                        UPDATE infra_ids 
                            SET dolar_id = @ID

                        INSERT INTO dolar
                        (
                            dolar_id,
                            tab_status_id, 
                            data, 
                            val_oficial, 
                            val_performance,
                            moeda_id
                        ) 
                        VALUES
                        (
                            @ID, 
                            1                              , 
                            @DATA                          , 
                            @VAL_OFICIAL                 , 
                            @VAL_PERFORMANCE,
                            @MOEDA_ID
                        ) 

	                     IF @@ROWCOUNT = 0
                            BEGIN
                                SELECT  0 id, 10 retorno, 'Nenhum registro afetado' mensagem 
                                RETURN 
                            END

                        IF @@ERROR <> 0
                            BEGIN
                                SELECT  0 id, error retorno, description mensagem 
                                FROM    master.dbo.sysmessages 
                                WHERE   error = @@error 
                                RETURN 
                            END

	                    SET @DESC_DOLAR = 'Inclusao dolar OFICIAL = ' + CONVERT(VARCHAR, @VAL_OFICIAL)
                        SET @DESC_DOLAR = @DESC_DOLAR + ', valor PERFORMANCE = ' + CONVERT(VARCHAR, @VAL_PERFORMANCE)

                        EXEC SP_SIG_INC_LOG_SISTEMA3 449, @zUSUARIO_ID, @ID, 1, @DESC_DOLAR, @RETORNO OUTPUT 
                        IF @RETORNO <> 0
                            BEGIN
                                SELECT  0 as id, @retorno retorno, 'Problemas na gravacao do registro de LOG' mensagem 
                                RETURN 
                            END

	                    SELECT 
                            @ID AS ID, 
                            0 AS retorno, 
                            '' AS MENSAGEM
                    END
                ELSE
                    BEGIN
                        SELECT 
		                    0 as id,
                            10 AS retorno, 
                            'Já existe Cotação para esta data.' AS mensagem
                    END
            ";
            using (var db = Connection)
            {
                retorno = db.Query<RetornoBD>
                    (
                        sql,
                        param: new
                        {
                            DATA            = dolar.data,
                            VAL_OFICIAL     = dolar.valOficial,
                            VAL_PERFORMANCE = dolar.valPerformance,
                            zUSUARIO_ID     = Global.UsuarioId,
                            MOEDA_ID        = dolar.moedaId
            }
                    );
                foreach(RetornoBD ret in retorno)
                {
                    if(ret.retorno == 0)
                    {
                        dolar.id = ret.id;
                        return true;
                    }
                    else
                    {
                        throw new Exception(ret.mensagem);
                    }
                }
            }//using
            return false;
        }//insert

        public bool updateDolar(DolarModel dolar)
        {
            IEnumerable<RetornoBD> retorno = null;
            string sql =
            @"
                DECLARE @RETORNO INT
                DECLARE @DESC_DOLAR VARCHAR(100)
                DECLARE @DUPLICIDADE TABLE ( QTD_DOLAR INT)
                DECLARE @QTD_DOLAR INT

                INSERT INTO @DUPLICIDADE EXEC SP_050_ECR_DUP_DOLAR @DATA, @DOLAR_ID, @MOEDA_ID
                select @QTD_DOLAR = QTD_DOLAR from @duplicidade

                IF @MOEDA_ID = 0 
                    BEGIN
                        SET @MOEDA_ID = NULL
                    END
                
                IF @QTD_DOLAR = 0 
                    BEGIN
                        SELECT 
                            @DESC_DOLAR = 'Valor atual dolar oficial = ' + CONVERT(VARCHAR, val_oficial) + ' sendo alterado para ' + CONVERT(VARCHAR, @VAL_OFICIAL)
                            + ', valor atual dolar performance = ' + CONVERT(VARCHAR, val_performance) + ' sendo alterado para ' + CONVERT(VARCHAR, @VAL_PERFORMANCE)
                        FROM dolar
                        WHERE dolar_id = @DOLAR_ID

                        UPDATE dolar
                            SET
                                TAB_STATUS_ID = 1, 
                                DATA = @DATA, 
                                VAL_OFICIAL = @VAL_OFICIAL, 
                                val_performance = @val_performance,
                                moeda_id    = @MOEDA_ID
                        WHERE dolar_id = @DOLAR_ID

                        IF @@ROWCOUNT = 0
                            BEGIN
                                SELECT  0 id, 10 retorno, 'Nenhum registro afetado' mensagem 
                                RETURN 
                            END

                        IF @@ERROR <> 0
                            BEGIN
                                SELECT  0 id, error retorno, description mensagem 
                                FROM    master.dbo.sysmessages 
                                WHERE   error = @@error 
                                RETURN 
                            END

                        EXEC SP_SIG_INC_LOG_SISTEMA3 449, @zUSUARIO_ID, @DOLAR_ID, 2, @DESC_DOLAR, @RETORNO OUTPUT 
                        IF @RETORNO <> 0
                            BEGIN
                                SELECT  0 id, @retorno retorno, 'Problemas na gravacao do registro de LOG' mensagem 
                                RETURN 
                            END

                        SELECT 
		                    @DOLAR_ID as id,
                            0 AS retorno, 
                            '' AS mensagem
                    END
                ELSE
                    BEGIN
                        SELECT 
		                    0 as id,
                            10 AS retorno, 
                            'Já existe Cotação para esta data.' AS mensagem
                    END
            ";
            using (var db = Connection)
            {
                retorno = db.Query<RetornoBD>
                    (
                        sql,
                        param: new
                        {
                            DOLAR_ID = dolar.id,
                            DATA = dolar.data,
                            VAL_OFICIAL = dolar.valOficial,
                            VAL_PERFORMANCE = dolar.valPerformance,
                            zUSUARIO_ID = Global.UsuarioId,
                            MOEDA_ID = dolar.moedaId
                        }
                    );
                foreach (RetornoBD ret in retorno)
                {
                    if (ret.retorno == 0)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception(ret.mensagem);
                    }
                }
            }//using
            return false;
        }//update

        public bool deleteDolar(DolarModel dolar)
        {
            IEnumerable<RetornoBD> retorno = null;
            string sql =
            @"
                DECLARE
                    @RETORNO INT 

                UPDATE DOLAR
                SET tab_status_id = 2 
                WHERE DOLAR_ID = @DOLAR_ID

                    if @@rowcount = 0
                        BEGIN
                            SELECT  0 id, 10 retorno, 'Nenhum registro afetado' mensagem 
                            RETURN 
                        END

                    if @@error <> 0
                        BEGIN
                            SELECT  error retorno, description msg_ret 
                            FROM    master.dbo.sysmessages 
                            WHERE   error = @@error 
                            RETURN 
                        END

                    EXEC SP_SIG_INC_LOG_SISTEMA2 449, @zUSUARIO_ID, 
                            @DOLAR_ID, 3, @RETORNO OUTPUT 
                    if @RETORNO <> 0
                        BEGIN
                            SELECT  0 id, @retorno retorno, 'Problemas na gravacao do registro de LOG' mensagem 
                            RETURN 
                        END


                SELECT 0 id, 0 retorno, '' mensagem
            ";
            using (var db = Connection)
            {
                retorno = db.Query<RetornoBD>
                    (
                        sql,
                        param: new
                        {
                            DOLAR_ID = dolar.id,
                            zUSUARIO_ID = Global.UsuarioId
                        }
                    );
                foreach (RetornoBD ret in retorno)
                {
                    if (ret.retorno == 0)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception(ret.mensagem);
                    }
                }
            }//using
            return false;
        }//delete
    }
}