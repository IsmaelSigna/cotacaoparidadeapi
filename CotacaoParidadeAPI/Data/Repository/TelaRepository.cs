﻿using CotacaoParidadeAPI.Data.Entities;
using CotacaoParidadeAPI.Data.Interface;
using Dapper;
using System.Collections.Generic;
using static CotacaoParidadeAPI.Data.Entities.Tela;

namespace CotacaoParidadeAPI.Data.Repository
{
    public class TelaRepository : RepositoryBase, ITelaRepository
    {
        public Tela carregarTela()
        {
            List<Tela> listTela = new List<Tela>();
            Tela tela = new Tela();

            using (var db = Connection)
            {
                string sqlFlag =
                   @"
                        SELECT DISTINCT
		                    flagExibeCampoMoeda.PARAMETRO AS flagExibeCampoMoeda,
                            flagCotacaoPorDia.PARAMETRO AS flagCotacaoPorDia
	                    FROM
		                    TAB_PARAMETRO_SISTEMA flagExibeCampoMoeda,
                            TAB_PARAMETRO_SISTEMA flagCotacaoPorDia
	                    WHERE
		                    flagExibeCampoMoeda.TAB_PARAMETRO_SISTEMA_ID = 8856 and
                            flagCotacaoPorDia.TAB_PARAMETRO_SISTEMA_ID = 8862
                   ";
                string sqlCampos =
                    @"
                        SELECT 
                            0 as id, '' as descricao
                        UNION
                        SELECT 
                            MOEDA_ID as id, MOEDA as descricao
                        FROM 
                            MOEDA 
                    ";
                listTela = (List<Tela>)db.Query<Tela>(sqlFlag);
                foreach (Tela t in listTela)
                {
                    tela = t;
                    tela.labelCampoValOficial = tela.flagExibeCampoMoeda == "S" ? "Valor Oficial" : "Valor BACEN";
                    tela.listMoeda = (List<Moeda>)db.Query<Moeda>(sqlCampos);
                }//foreach  
            }//using
            return tela;
        }//carregarTela
    }
}