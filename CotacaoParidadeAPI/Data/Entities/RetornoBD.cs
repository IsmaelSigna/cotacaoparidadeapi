﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CotacaoParidadeAPI.Data.Entities
{
    public class RetornoBD
    {
        public int      id          { get; set; }
        public int      retorno     { get; set; }
        public string   mensagem    { get; set; }
    }
}