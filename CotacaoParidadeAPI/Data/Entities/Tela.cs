﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CotacaoParidadeAPI.Data.Entities
{
    public class Tela
    {
        public string       labelCampoValOficial    { get; set;  }
        public string       flagExibeCampoMoeda     { get; set; }
        public string       flagCotacaoPorDia       { get; set; }
        public List<Moeda>  listMoeda               { get; set; }

        public class Moeda
        {
            public int id { get; set; }
            public string descricao { get; set; }
        }
    }
}