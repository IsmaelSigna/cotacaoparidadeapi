﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CotacaoParidadeAPI.Data.Entities
{
    public class Dolar
    {
        public int?     id              { get; set; }
        public int?     tabStatusId     { get; set; }
        public DateTime data            { get; set; }
        public float?   valorTurismo    { get; set; }
        public float?   valorParalelo   { get; set; }
        public float?   valorOficial    { get; set; }
        public float?   valPerformance  { get; set; }
        public float?   valOficial      { get; set; }
        public int?     moedaId         { get; set; }
        public string moedaDesc { get; set; }
    }
}