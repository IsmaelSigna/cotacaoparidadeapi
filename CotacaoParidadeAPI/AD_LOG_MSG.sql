If Not Exists (Select 1 From sys.columns Where Name = 'funcao_id' And Object_Id = Object_Id('Log_Msg'))
	Alter Table [Log_Msg] Add [funcao_id] INT  Null
Else 
	If Not Exists(Select * From sys.columns c Where c.[object_id] = Object_Id('Log_Msg') And c.Name = 'funcao_id' And Type_Name(c.system_type_id) = 'int' And c.max_length = 4 And c.[precision] = 10 And c.scale = 0)
		Alter Table [Log_Msg] Alter Column [funcao_id] INT Null
Go