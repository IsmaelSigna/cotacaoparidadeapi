﻿using Signa.Library.Helpers;
using Signa.Library.Library;
using System;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CotacaoParidadeAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            //config.Filters.Add()

            string nomeApi = ConfigurationManager.AppSettings["NomeApi"];
            if (!StringLibrary.ConsistirStringVazia(nomeApi))
            {
                nomeApi = "NomePadrãoApi";
            }
            Global.NomeApi = nomeApi;

            string sFuncaoId = ConfigurationManager.AppSettings["FuncaoId"];

            if (StringLibrary.ConsistirStringVazia(sFuncaoId))
            {
                Global.FuncaoId = Int32.Parse(sFuncaoId);
            }
            else
            {
                Global.FuncaoId = 0; //colocar função
            }

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
