﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CotacaoParidadeAPI.Models
{
    public class DolarModel
    {
        public int? id { get; set; }
        public int? tabStatusId { get; set; }
        [Required(ErrorMessage = "Informar Data.", AllowEmptyStrings = false)]
        public DateTime data { get; set; }
        public float? valorTurismo { get; set; }
        public float? valorParalelo { get; set; }
        public float? valorOficial { get; set; }
        public float? valPerformance { get; set; }
        public float? valOficial { get; set; }
        public int? moedaId { get; set; }
        public string moedaDesc { get; set; }

        public bool validar(ref string mensagem)
        {
            if(this.moedaId == 0 || this.moedaId == null)
            {
                mensagem = "Selecione uma Moeda para a Cotação.";
                return false;
            }

            if (this.data == null)
            {
                mensagem = "Selecione uma Data de Cotação.";
                return false;
            }

            if (this.valOficial == null || this.valOficial == 0)
            {
                mensagem = "Selecione um Valor Oficial para a Cotação.";
                return false;
            }
            return true;
        }

    }
}