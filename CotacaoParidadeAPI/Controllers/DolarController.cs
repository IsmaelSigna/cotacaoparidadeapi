﻿using CotacaoParidadeAPI.Data.Entities;
using CotacaoParidadeAPI.Data.Interface;
using CotacaoParidadeAPI.Data.Repository;
using CotacaoParidadeAPI.Filters;
using CotacaoParidadeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace CotacaoParidadeAPI.Controllers
{
    [ExceptionFilter]
    [AuthorizateAttribute]
    [RoutePrefix("CotacaoParidade")]
    public class DolarController : ApiController
    {
        IDolarRepository _dolar;
        ITelaRepository _tela;
        string mensagem;

        DolarController()
        {
            this._dolar = new DolarRepository();
            this._tela = new TelaRepository();
        }

        [HttpGet]
        [Route("CarregarTela")]
        public IHttpActionResult CarregarTela()
        {
            Tela tela = null;

            try
            {
                tela = _tela.carregarTela();
            }
            catch (Exception ex)
            {
                return BadRequest("Erro ao consultar a API. " + ex.Message);
            }

            return Ok(tela);
        }

        [HttpPost]
        [Route("Buscar")]
        public IHttpActionResult Buscar(DolarModel dolar)
        {
            List<Dolar> listDolar = null;
            try
            {
                listDolar = (List<Dolar>)_dolar.searchDolar(dolar);
            }
            catch (Exception ex)
            {
                return BadRequest("Erro ao consultar a API. " + ex.Message);
            }

            return Ok(listDolar);

        }
        
        [HttpPost]
        [Route("Gravar")]
        public IHttpActionResult Gravar(DolarModel dolar)
        {

            if (!ModelState.IsValid)
            {
                HttpRequestMessage Request = new HttpRequestMessage();
                return BadRequest(ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
                    + ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().Exception.Message);
            }
            if (!dolar.validar(ref this.mensagem)) return BadRequest(this.mensagem);

            try
            {
                _dolar.insertDolar(dolar);
                return Ok(dolar);        
            }
            catch (Exception ex)
            {
                return BadRequest("Não foi possível inserir a Cotação. " + ex.Message);
            }
        }

        [HttpPost]
        [Route("Atualizar")]
        public IHttpActionResult Atualizar(DolarModel dolar)
        {
            if (!ModelState.IsValid)
            {
                HttpRequestMessage Request = new HttpRequestMessage();
                return BadRequest(ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
                    + ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().Exception.Message);
            }
            if (dolar.id == 0)
            {
                return BadRequest("Informe um id válido");
            }
            if (!dolar.validar(ref this.mensagem)) return BadRequest(this.mensagem);

            try
            {
                _dolar.updateDolar(dolar);
                return Ok(dolar);
            }
            catch (Exception ex)
            {
                return BadRequest("Não foi possível atualizar a Cotação. " + ex.Message);
            }

        }

        [HttpPost]
        [Route("Excluir")]
        public IHttpActionResult Excluir(DolarModel dolar)
        {
            if (!ModelState.IsValid)
            {
                HttpRequestMessage Request = new HttpRequestMessage();
                return BadRequest(ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
                    + ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().Exception.Message);
            }
            if (dolar.id == 0)
            {
                return BadRequest("Informe um id válido");
            }

            try
            {
                _dolar.deleteDolar(dolar);
                return Ok(dolar);
            }
            catch (Exception ex)
            {
                return BadRequest("Não foi possível atualizar a Cotação. " + ex.Message);
            }

        }
    }
}